package sematec.sematec_tamrin2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String all_text_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText enter_name = (EditText) findViewById(R.id.enter_name);
        final EditText enter_family = (EditText) findViewById(R.id.enter_family);
        final TextView all_text = (TextView) findViewById(R.id.all_text);
        Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
       all_text_string =   enter_name.getText().toString() + " " + enter_family.getText().toString() ;
                all_text.setText(all_text_string);
            }
        });
            }
        }

